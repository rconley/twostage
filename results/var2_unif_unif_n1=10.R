# Sample 1 has var = 1
# Sample 2 has var = 2
# n1 = 10

# Load ggplot2
library(ggplot2)

# Data
n <- rep(c(10,20,30,40,50),5) # sample sizes, written 5 times (one for each test)

error_two = c( 0.0548 , 0.0435 , 0.0384 , 0.0312 , 0.0236 )
error_ttest = c( 0.0546 , 0.0495 , 0.0519 , 0.054 , 0.0549 )
error_wilcoxon = c( 0.0504 , 0.0308 , 0.0243 , 0.0208 , 0.0171 )
error_boot = c( 0.0516 , 0.0389 , 0.04 , 0.0453 , 0.0475 )
alpha = c(0.05, 0.05, 0.05, 0.05, 0.05) # alpha = 0.05
errors = c(error_two, error_ttest, error_wilcoxon, error_boot, alpha)

# Store the test types
test_type = c(rep("Two-Stage", 5), rep("t-Test", 5), 
              rep("Wilcoxon", 5), rep("Bootstrap", 5),
              rep("alpha = 0.05", 5))

# Make a data frame
df <- data.frame(n, errors, test_type)

# Reording the labels
df$test_type <- factor(df$test_type, levels = c("Two-Stage", "t-Test", "Wilcoxon", "Bootstrap", "alpha = 0.05"))


# Create plot (naming the plot "p" )
p <- ggplot(data = df, aes(x=n, y=errors)) +
  geom_line(aes(colour=test_type, linetype = test_type)) +
  ylim(0, 0.1)

# Editing the labels:
p <- p + labs(title = "Type I Error when Sample Size 1 is 10",
              colour = "Test Type", 
              linetype = "Test Type",
              x = "Sample Size of Sample 2", y = "Estimated Type I Error")

p <- p + labs(subtitle = expression(paste(
  "Sample 1 from uniform dist, ", sigma^{2},'= 1. Sample 2 from uniform dist, ', sigma^{2},'= 2.' )))

# Setting line type
p <- p + scale_linetype_manual(values=c("solid", "longdash", "twodash", "dashed", "dotted"))

# Setting colors manually
p <- p + scale_colour_manual(values = c("red", "blue", "green", "tan3", "black"))

# Print the plot:
print(p)