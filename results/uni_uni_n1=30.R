# Both samples are from the normal distribution.
# n1 = 10

# Load ggplot2
library(ggplot2)

# Data
n <- rep(c(10,20,30,40,50),5) # sample sizes, written 5 times (one for each test)

error_two = c(0.0534, 0.0492, 0.0531, 0.0504, 0.0459) # two stage test
error_ttest = c(0.0542, 0.0489, 0.0536, 0.05, 0.0466) # t-test
error_wilcoxon = c(0.0499, 0.0487, 0.052 ,0.0496, 0.046) # Wilcoxon
error_boot = c(0.0477, 0.046, 0.0529 ,0.0484 ,0.0452) # bootstrap
alpha = c(0.05,0.05,0.05,0.05,0.05) # alpha = 0.05
errors = c(error_two, error_ttest, error_wilcoxon, error_boot, alpha)

# Store the test types
test_type = c(rep("Two-Stage", 5), rep("t-Test", 5), 
              rep("Wilcoxon", 5), rep("Bootstrap", 5),
              rep("alpha = 0.05", 5))

# Make a data frame
df <- data.frame(n, errors, test_type)

# Reording the labels
df$test_type <- factor(df$test_type, levels = c("Two-Stage", "t-Test", "Wilcoxon", "Bootstrap", "alpha = 0.05"))


# Create plot (naming the plot "p" )
p <- ggplot(data = df, aes(x=n, y=errors)) +
  geom_line(aes(colour=test_type, linetype = test_type)) +
  ylim(0, 0.1)

# Editing the labels:
p <- p + labs(title = "Type I Error when Sample Size 1 is 30   ",
              #subtitle = "Sample 1 from Normal. Sample 2 from Normal",
              colour = "Test Type", 
              linetype = "Test Type",
              x = "Sample Size of Sample 2", y = "Estimated Type I Error")

p <- p + labs(subtitle = expression(paste(
  "Sample 1 from Uniform dist, ", sigma^{2},'= 1. Sample 2 from Uniform dist, ', sigma^{2},'= 1.' )))

# Setting line type
p <- p + scale_linetype_manual(values=c("solid", "longdash", "twodash", "dashed", "dotted"))

# Setting colors manually
p <- p + scale_colour_manual(values = c("red", "blue", "green", "tan3", "black"))

# Print the plot:
print(p)