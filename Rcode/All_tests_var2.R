# Two stage test (Table 3 in Rochon)

# time the test
ptm <- proc.time()

set.seed(729)  
library(LaplacesDemon)
library(resample)

# Number of iterations of the tests
k <- 10000

# Sample Sizes
n1 = 50
# n2 is set in loop - see line 33
  
# alpha for the two-stage test
alpha_pre = 0.05

# vectors to store type I error for different sample sizes n2
errors_t = c()
errors_2 = c()
errors_W = c()
errors_B = c()

# Parameters for the mix-normal distribution
sigma <- c(sqrt(0.75), sqrt(13.25))
p1 <- c(0.98, 0.02)
p2 <- c(0.9, 0.1)
mu <- c(0,0)
  
# loop over different sample sizes for n2
for (j in 1:5){
  # sample size 2
  n2 = 10*j 
  
  # counters for correct tests  
  correct_t <- 0     # all t-tests
  correct_2 <- 0     # two-stage tests
  correct_W <- 0     # all Wilcoxen tests
  correct_B <- 0     # all bootstrap tests
  
  # choose many samples to estimate Type I error
  for (i in 1:k) {
    x1 = rnorm(n1)
    #x2 = rnorm(n2, s = sqrt(2))
    #x1 = runif(n1, min=-sqrt(3), max=sqrt(3))
    x2 = runif(n2, min=-sqrt(6), max=sqrt(6))
    #x1 = rnormm(n1, p1, mu, sigma)
    #x2 = rnormm(n2, p2, mu, sigma)
 
    if (i%%100 == 0){
      cat(n2,' ', i, '\n') 
    }
      
    # Do the two-stage test
    pval1 = shapiro.test(x1)
    pval2 = shapiro.test(x2)
    if(pval1$p.value > alpha_pre & pval2$p.value > alpha_pre){
      # if both samples are normal, run a two-sample t-test
      ttest=t.test(x1, x2, paired = FALSE, var.equal = FALSE)
      if(ttest$p.value > 0.05) {
        correct_2 <- correct_2 + 1
      }
    }
    else{
      # if one of the samples isn't normal, run a Mann-Whitney U (Wilcoxon) test
      wtest=wilcox.test(x1,x2)
      if(wtest$p.value > 0.05) {
        correct_2 <- correct_2 + 1
      }
    }
    
    
    # Apply t-test (without pre-testing)
    ttest=t.test(x1, x2, paired = FALSE, var.equal = FALSE)
    if(ttest$p.value > 0.05) {
      correct_t <- correct_t + 1
    }
    
    
    # Apply Wilcoxon test (without pre-testing)
    wtest=wilcox.test(x1,x2)
    if(wtest$p.value > 0.05) {
      correct_W <- correct_W + 1
    }
    
    
    # Apply Bootstrap
    B = bootstrap2(data = x1, mean, data2 =x2)
    max = CI.t(B, probs = c(0.025,0.975))
    if(max[1] < 0 & 0< max[2]){
      # Check to see if 0 is between max[1] or max[2]
      correct_B <- correct_B + 1
    }
  }
  
  # record the type I error for n2
  errors_2[j] = 1 - correct_2/k
  errors_t[j] = 1 - correct_t/k
  errors_W[j] = 1- correct_W/k
  errors_B[j] = 1 - correct_B/k
  
  cat("error_two = c(",errors_2[1],",",errors_2[2],",",errors_2[3],",",errors_2[4],",",errors_2[5],")\n")
  cat("error_ttest = c(",errors_t[1],",",errors_t[2],",",errors_t[3],",",errors_t[4],",",errors_t[5],")\n")
  cat("error_wilcoxon = c(",errors_W[1],",",errors_W[2],",",errors_W[3],",",errors_W[4],",",errors_W[5],")\n")
  cat("error_boot = c(",errors_B[1],",",errors_B[2],",",errors_B[3],",",errors_B[4],",",errors_B[5],")\n")
  
}

cat("Type I error for two-stage: ", errors_2)
cat("\n")
cat("Type I error for t-test: ", errors_t)
cat("\n")
cat("Type I error for Wilcoxon: ", errors_W)
cat("\n")
cat("Type I error for bootstrap: ", errors_B)
cat("\n")


# Stop the clock
cat("Time to run this program in seconds: \n")
time = proc.time() - ptm
cat(time[3])
